package saltapi

import (
	"context"

	"github.com/hashicorp/terraform-plugin-sdk/v2/diag"
	"github.com/hashicorp/terraform-plugin-sdk/v2/helper/schema"
	salt "github.com/xuguruogu/go-salt"
)

type createKeyRequest struct {
	id_ 	string	 
}


func resourceWheelKey() *schema.Resource {
	return &schema.Resource{
		CreateContext: resourceWheelKeyCreate,
		DeleteContext: resourceWheelKeyDelete,
		UpdateContext: resourceWheelKeyUpdate,
		ReadContext:   resourceWheelKeyRead,
		ExistsContext: resourceWheelKeyExists,
		Schema: map[string]*schema.Schema{
			"minion_id": {
				Type:        schema.TypeString,
				Optional:    true,
				Description: "must match exactly one minion",
			},
			"force": {
				Type:     schema.TypeBool,
				Optional: true,
			},
			"keysize": {
				Type:     schema.TypeInt,
				Optional: true,
				ForceNew: true,
			},
		},
	}
}


func makeApiRequest(meta interface{}, method string, args ) {
	client := meta.(salt.Client)
	client.SetClient("wheel")

	var diags diag.Diagnostics
	
	apiReturn := make(map[string][]string)
	err := client.RunCmd("*", "key.accept", nil, args, &apiReturn)	
}

func resourceWheelKeyCreate(ctx context.Context, d *schema.ResourceData, meta interface{}) diag.Diagnostics {

	// Warning or errors can be collected in a slice type
	var diags diag.Diagnostics

	args := make(map[string]interface{})
	args["_id"] = d.Get("minion_id")
	args["keysize"] = d.Get("keysize")
	args["force"] = d.Get("force")

	makeApiRequest(meta, requestMethod, args)

	//log.Printf("[DEBUG] create gitlab deployment key %s", *options.Title)

	if err != nil {
		return diag.FromErr(err)
	}

	d.setId(args["_id"])
	return diags
}

func resourceWheelKeyUpdate(ctx context.Context, d *schema.ResourceData, meta interface{}) diag.Diagnostics {
	client := meta.(salt.Client)
	client.SetClient("wheel")

	// Warning or errors can be collected in a slice type
	var diags diag.Diagnostics
	return diags
}

func resourceWheelKeyRead(ctx context.Context, d *schema.ResourceData, meta interface{}) diag.Diagnostics {
	client := meta.(salt.Client)
	client.SetClient("wheel")

	// Warning or errors can be collected in a slice type
	var diags diag.Diagnostics
	return diags
}

func resourceWheelKeyDelete(ctx context.Context, d *schema.ResourceData, meta interface{}) diag.Diagnostics {
	client := meta.(salt.Client)
	client.SetClient("wheel")

	// Warning or errors can be collected in a slice type
	var diags diag.Diagnostics
	return diags
}

func resourceWheelKeyExists(ctx context.Context, d *schema.ResourceData, meta interface{}) diag.Diagnostics {


}