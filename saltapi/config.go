package saltapi

import (
	"crypto/tls"
	"net/http"

	"github.com/xuguruogu/go-salt"
)

// Config is per-provider, specifies where to connect to gitlab
type Config struct {
	User     string
	Password string
	Eauth    string
	Insecure bool
	Url      string
}

// Client returns a *salt.Client to interact with the configured salt api instance
func (c *Config) Client() (salt.Client, error) {
	// Configure TLS/SSL
	tlsConfig := &tls.Config{}

	// If configured as insecure, turn off SSL verification
	if c.Insecure {
		tlsConfig.InsecureSkipVerify = true
	}

	t := http.DefaultTransport.(*http.Transport).Clone()
	t.TLSClientConfig = tlsConfig
	t.MaxIdleConnsPerHost = 100

	client := salt.NewClient(c.Url, c.User, c.Password)
	client.SetSSLSkipVerify(c.Insecure)

	return client, nil
}
