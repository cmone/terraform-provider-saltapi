package saltapi

import (
	"os"
	"testing"

	"github.com/hashicorp/terraform-plugin-sdk/v2/helper/schema"
)

var testAccProviders map[string]*schema.Provider
var testAccProvider *schema.Provider

func init() {
	testAccProvider = Provider()
	testAccProviders = map[string]*schema.Provider{
		"saltapi": testAccProvider,
	}
}

func TestProvider(t *testing.T) {
	if err := Provider().InternalValidate(); err != nil {
		t.Fatalf("err: %s", err)
	}
}

func TestProvider_impl(t *testing.T) {
	var _ *schema.Provider = Provider()
}

func testAccPreCheck(t *testing.T) {
	if err := os.Getenv("SALTAPI_USER"); err == "" {
		t.Fatal("SALTAPI_USER must be set for acceptance tests")
	}
	if err := os.Getenv("SALTAPI_PASSWORD"); err == "" {
		t.Fatal("SALTAPI_PASSWORD must be set for acceptance tests")
	}
	if err := os.Getenv("SALTAPI_URL"); err == "" {
		t.Fatal("SALTAPI_URL must be set for acceptance tests")
	}
}
