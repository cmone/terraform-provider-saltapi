package saltapi

import (
	"context"

	"github.com/hashicorp/terraform-plugin-sdk/v2/diag"
	"github.com/hashicorp/terraform-plugin-sdk/v2/helper/schema"
)

// Provider - Terrafrom properties for proxmox
func Provider() *schema.Provider {

	return &schema.Provider{

		Schema: map[string]*schema.Schema{
			"salt_api_user": {
				Type:        schema.TypeString,
				Required:    true,
				DefaultFunc: schema.EnvDefaultFunc("SALTAPI_USER", nil),
				Description: "username",
			},
			"salt_api_password": {
				Type:        schema.TypeString,
				Required:    true,
				DefaultFunc: schema.EnvDefaultFunc("SALTAPI_PASSWORD", nil),
				Description: "secret",
				Sensitive:   true,
			},
			"salt_api_eauth": {
				Type:        schema.TypeString,
				Optional:    true,
				DefaultFunc: schema.EnvDefaultFunc("SALTAPI_EAUTH", nil),
				Description: "eauth param, defaults to 'pam'",
			},
			"salt_api_url": {
				Type:        schema.TypeString,
				Required:    true,
				DefaultFunc: schema.EnvDefaultFunc("SALTAPI_URL", nil),
				Description: "https://host.fqdn:8443",
			},
			"salt_api_insecure": {
				Type:        schema.TypeBool,
				Optional:    true,
				DefaultFunc: schema.EnvDefaultFunc("SALTAPI_INSECURE", false),
				Description: "skip ssl cert verification",
			},
			"salt_timeout": {
				Type:     schema.TypeInt,
				Optional: true,
				Default:  300,
			},
		},

		ResourcesMap: map[string]*schema.Resource{
			"saltapi_wheel_key": resourceWheelKey(),
		},
		/*DataSourcesMap: map[string]*schema.Resource{
			"saltapi_wheel_key_list":        dataSourceWheelKeyList(),
			"saltapi_wheel_key_minion":      dataSourceWheelKeyMinion(),
			"saltapi_wheel_key_master":      dataSourceWheelKeyMaster(),
			"saltapi_wheel_key_fingerprint": dataSourceWheelKeyFingerprint(),
		},*/
		ConfigureContextFunc: providerConfigure,
	}
}

func providerConfigure(ctx context.Context, d *schema.ResourceData) (interface{}, diag.Diagnostics) {
	config := Config{
		User:     d.Get("salt_api_user").(string),
		Password: d.Get("salt_api_password").(string),
		Eauth:    d.Get("salt_api_eauth").(string),
		Insecure: d.Get("salt_api_insecure").(bool),
		Url:      d.Get("salt_api_url").(string),
	}

	// Warning or errors can be collected in a slice type
	var diags diag.Diagnostics

	client, err := config.Client()
	if err != nil {
		return nil, diag.FromErr(err)
	}

	return client, diags
}
