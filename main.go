package main

import (
	"github.com/chrimi/terraform-provider-saltapi/saltapi"
	"github.com/hashicorp/terraform-plugin-sdk/plugin"
)

func main() {
	plugin.Serve(&plugin.ServeOpts{
		ProviderFunc: saltapi.Provider})
}
