module github.com/chrimi/terraform-provider-saltapi

go 1.15

require (
	github.com/hashicorp/terraform v0.13.5
	github.com/hashicorp/terraform-plugin-sdk v1.16.0
	github.com/hashicorp/terraform-plugin-sdk/v2 v2.2.0
	github.com/xuguruogu/go-salt v0.0.0-20161028135252-551de499c15a
	github.com/xuguruogu/gorest v0.0.0-20161104061311-90aadd130417 // indirect
)
